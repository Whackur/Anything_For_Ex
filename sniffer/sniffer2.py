from socket import *
import os
import struct

def parse_ipheader(data):
    ipheader = struct.unpack("!BBHHHBBH4s4s", data[:20])
    return ipheader

def getDatagramSize(ipheader):
    return ipheader[2]

def getProtocol(ipheader):
    protocols = { 1: "ICMP", 6: "TCP", 17: "UDP"}
    proto = ipheader[6]
    if proto in protocols:
        return protocols[proto]
    else:
        return "OTHERS"

def getIP(ipheader):
    src_ip = inet_ntoa(ipheader[8])
    dest_ip = inet_ntoa(ipheader[9])
    return (src_ip, dest_ip)


def recvData(sock):
    data = ""
    try:
        data = sock.recvfrom(65535)
    except timeout:
        data = ''
        
    return data[0]
    

def getMyIp():
    # 실제 외부 통신시 사용하는 IP 획득
    myIp = ''
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    myIp = s.getsockname()[0]
    return myIp


def sniffing(host):
    if os.name == "nt":
        sock_protocol = IPPROTO_IP
    else:
        sock_protocol = IPPROTO_ICMP

    # RAW 소켓을 만들고 host에 bind함. setsockopt를 이용하여 가로채는 패킷에 IP 헤더 포함
    sniffer = socket(AF_INET, SOCK_RAW, sock_protocol)
    sniffer.bind((host, 0))
    sniffer.setsockopt(IPPROTO_IP, IP_HDRINCL, 1)
    
    # 윈도우의 경우 소켓을 promiscuous 모드로 변경
    if os.name == "nt":
        sniffer.ioctl(SIO_RCVALL, RCVALL_ON)
        print(sniffer.recvfrom(65535))

    if os.name == 'nt':
        sniffer.ioctl(SIO_RCVALL, RCVALL_OFF)

    count = 1
    try:
        while True:
            data = recvData(sniffer)
            ipheader = parse_ipheader(data[:20])
            datagramSize = getDatagramSize(ipheader)
            protocol = getProtocol(ipheader)
            src_ip, dest_ip = getIP(ipheader)
            print('\nSNIFFED [%d] ++++++++++++++' %count)
            print('Datagram SIZE:\t%s' %str(datagramSize))
            print('Protocol:\t%s' %protocol)
            print('Source IP:\t%s' %src_ip)
            print('Destination IP:\t%s' %dest_ip)
            count += 1
    except KeyboardInterrupt : # Ctrl - C
        if os.name == "nt":
            sniffer.ioctl(SIO_RCVALL, RCVALL_OFF)

def main():
    host = getMyIp()
    # host = gethostbyname(gethostname())
    print('START SNIFFING at [%s]' % host)
    sniffing(host)


if __name__ == '__main__':
    main()
