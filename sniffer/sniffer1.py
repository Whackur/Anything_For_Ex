from socket import *
import os


def recvData(sock):
    data = ""
    try:
        data = sock.recvfrom(65535)
    except timeout:
        data = ''
        
    return data[0]
    

def getMyIp():
    # 실제 외부 통신시 사용하는 IP 획득
    myIp = ''
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    myIp = s.getsockname()[0]
    return myIp


def sniffing(host):
    if os.name == "nt":
        sock_protocol = IPPROTO_IP
    else:
        sock_protocol = IPPROTO_ICMP

    # RAW 소켓을 만들고 host에 bind함. setsockopt를 이용하여 가로채는 패킷에 IP 헤더 포함
    sniffer = socket(AF_INET, SOCK_RAW, sock_protocol)
    sniffer.bind((host, 0))
    sniffer.setsockopt(IPPROTO_IP, IP_HDRINCL, 1)
    
    # 윈도우의 경우 소켓을 promiscuous 모드로 변경
    if os.name == "nt":
        sniffer.ioctl(SIO_RCVALL, RCVALL_ON)
        print(sniffer.recvfrom(65535))

    if os.name == 'nt':
        sniffer.ioctl(SIO_RCVALL, RCVALL_OFF)

    count = 1
    try:
        while True:
            data = recvData(sniffer)
            print(data)
            print("Sniffed [%d] %s\n" %(count, data[:20]))
            count += 1
    except KeyboardInterrupt : # Ctrl - C
        if os.name == "nt":
            sniffer.ioctl(SIO_RCVALL, RCVALL_OFF)

def main():
    # host = getMyIp()
    host = gethostbyname(gethostname())
    print('START SNIFFING at [%s]' % host)
    sniffing(host)


if __name__ == '__main__':
    main()
