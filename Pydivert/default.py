import pydivert

with pydivert.WinDivert("tcp.SrcPort == 443 and tcp.PayloadLength==0") as w:
    num = 0
    for packet in w:
        if packet.tcp.rst == True:
            num += 1
            print(str(num) + ". RST Packet from : " + packet.src_addr)
            packet.tcp.rst = False
            print(packet.tcp.rst)
        w.send(packet)