import pydivert

with pydivert.WinDivert("tcp.SrcPort == 443 and tcp.PayloadLength==0") as w:
    for packet in w:
        if packet.tcp.rst == False:
            # print(packet, end="\n")
            w.send(packet)
        elif packet.tcp.rst == True:
            packet.tcp.rst = False
            print("RST Packet from : " + packet.src_addr, end="\n")
            print(packet.tcp.rst)
        else:
            pass
        # except Exception as e:
        #     print(e)