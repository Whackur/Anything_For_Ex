import pydivert

with pydivert.WinDivert("inbound and tcp.Rst") as w:
    for packet in w:
        packet.tcp.rst=False
        w.send(packet)