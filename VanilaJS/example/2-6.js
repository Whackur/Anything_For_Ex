const title = document.querySelector("#title");

const BASE_COLOR = "rgb(52,73,94)";
const OTHER_COLOR = "rgb(0,0,0)";

function handleClick(){
    alert(45)
    const currentColor = title.style.color;
    if (currentColor === BASE_COLOR){
        title.style.color = OTHER_COLOR;
    }
    else {
        title.style.color = BASE_COLOR;
    }
    
}

function init(){
    title.style.color = BASE_COLOR;
    title.addEventListener("click",handleClick);
}

init();

function handleOffline(){
    console.log("Wifi turned off")
}

function handleOnline(){
    console.log("Wifi TURNNED ON!!")
}
window.addEventListener("offline", handleOffline);
window.addEventListener("online", handleOnline);

